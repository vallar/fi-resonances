function [data]=boundaries_COM(shot, time,E, varargin)
%% Function to compute the boundaries in Constants of motion
% INPUT
%   REQUIRED
%   shot (int): shot to use to compute the boundaries
%   time (float): time of interest
%   E (float): Energy in kev
%
%   OPTIONAL
%   doplot (bool) (1): set to 0 to shut the plots
%   axx (obj) ([]): axis object used to overplot
%   eqdsk_in (obj) ([]): input eqdsk object to use
%   color_in (char) ('k'): color to plot
%   epot (float) (0): energy to use to compute the boundaries with an
%   additional term
%   norm (bool) (0): set to 1 if normalized quantitites are wanted
%
% OUTPUT
%   structure with relevant quantities


%% Parse and check arguments
p = inputParser;
% Required
addRequired(p,'shot', @isnumeric);
addRequired(p,'time', @isnumeric);
addRequired(p,'E', @isnumeric);
% Optional
addParameter(p,'doplot',1, @isnumeric);
addParameter(p,'axx',[]);
addParameter(p,'eqdsk_in',[], @isstruct);
addParameter(p,'color_in','k', @ischar);
addParameter(p,'epot',0, @isnumeric);
addParameter(p,'norm',0, @isnumeric);

parse(p, shot, time, E, varargin{:})
p = p.Results;
color_in=p.color_in;
if isempty(p.eqdsk_in)
  eqdsk=gdat(shot, 'eqdsk', 'time', time);
  [eqdsk_cocosout]=eqdsk_cocos_transform(eqdsk.eqdsk,[17,3],1,1);
else
  eqdsk_cocosout=p.eqdsk_in;
end
if E>1
  E = E*1.602e-19;
end
data.p = p;
data.eqdsk = eqdsk_cocosout;
data.E = E;
%% Read eqdsk and interpolate on correct axis
% ad-hoc cocos conversion
psiedge = eqdsk_cocosout.psiedge;
psiaxis = eqdsk_cocosout.psiaxis;
psiedge=psiedge-psiaxis;
psiaxis=0;
if psiaxis>psiedge
  psiedge = -1.*psiedge;
end
% Compute B
r0 = eqdsk_cocosout.r0;
b0 = abs(eqdsk_cocosout.b0);
r=linspace(min(eqdsk_cocosout.rplas), max(eqdsk_cocosout.rplas), 100);
b_axis = b0*r0./eqdsk_cocosout.raxis;
% computing rho_pol_norm and interpolate g on that
rho_pol_norm_grid = eqdsk_cocosout.rhopsi;
g=eqdsk_cocosout.F;
rho_pol_norm_2d = sqrt((eqdsk_cocosout.psi-eqdsk_cocosout.psiaxis)./(eqdsk_cocosout.psiedge-eqdsk_cocosout.psiaxis));
g_2d = interpn(rho_pol_norm_grid, abs(g), rho_pol_norm_2d);
% Find g on the midplane and rho_pol_norm on the midplane
z = eqdsk_cocosout.zmesh;
[~, ind_z0] = min(abs(z-eqdsk_cocosout.zaxis));
ind_r = eqdsk_cocosout.rmesh<max(r)& eqdsk_cocosout.rmesh>min(r);
rho_pol_norm_z0 = rho_pol_norm_2d(ind_r, ind_z0);
[~, ind_min_rho_pol_norm] = min(abs(rho_pol_norm_z0));
rho_pol_norm_z0(ind_min_rho_pol_norm) = 0;
g_z0 = g_2d(ind_r, ind_z0);
min_rho_g=0;
g_axis=nan;
while isnan(g_axis)
  g_axis = interp1q(rho_pol_norm_z0, g_z0, min_rho_g);
  min_rho_g=min_rho_g+0.01;
end
%% compute B_phi from F function
b = g_z0./eqdsk_cocosout.rmesh(ind_r);
[~, ind_r_axis] = min(abs(eqdsk_cocosout.rmesh(ind_r)-eqdsk_cocosout.raxis));
b_axis = b(ind_r_axis);

%% Find Pphi range
A=2;
mass = A*1.66e-27;
vmax = sqrt(2*E./mass);
Pphi_max = mass*max(r)*vmax+1.6e-19*psiedge;
Pphi_min = -2*Pphi_max;
Pphi = linspace(Pphi_min, Pphi_max, 100);
data.Pphi = Pphi;
%% compute boundaries
%% passing
% axis
lost_lfs   = E./min(b)  -  (1./(2*mass*min(g).^2)*min(b)*(Pphi+1.602e-19*psiedge).^2); 
lost_hfs   = E./max(b)  -  (1./(2*mass*min(g).^2)*max(b)*(Pphi+1.602e-19*psiedge).^2); 
touch_axis = E./b_axis  -  (1./(2*mass*g_axis.^2)*b_axis*(Pphi+1.602e-19*psiaxis).^2); 
data.lost_lfs = lost_lfs;
data.lost_hfs = lost_hfs;
data.touch_axis = touch_axis;

%% trapped. The formulas used have as x the Pphi*q_e, as y the mu normalized
psi_midplane = psiedge.*rho_pol_norm_z0.^2;
% this is to have psi_midplane and b in the same shape
psi_midplane = interp1(linspace(0,1,numel(psi_midplane)), psi_midplane', linspace(0,1,numel(b)));
ind_psi_0 = find(psi_midplane==min(psi_midplane));
x_uptrapp = -1.*psi_midplane(ind_psi_0:end);
x_uptrapp = x_uptrapp*1.602e-19;
y_uptrapp = b_axis./b(ind_psi_0:end); 
y_uptrapp = y_uptrapp*E./b_axis;
x_downtrapp = -1.*psi_midplane(1:ind_psi_0);
x_downtrapp = x_downtrapp*1.602e-19;
y_downtrapp=b_axis./b(1:ind_psi_0);
y_downtrapp = y_downtrapp*E./b_axis;

data.uptrapp.x = x_uptrapp;
data.uptrapp.y = y_uptrapp;
data.downtrapp.x = x_downtrapp;
data.downtrapp.y = y_downtrapp;
%% Max Pphi for each mu (potato orbits)
npitch=5000;
mu_for_max = zeros(1, npitch*numel(find(ind_r)));
pphi_for_max = zeros(1, npitch*numel(find(ind_r)));
for ipitch=1:npitch
  for ir = 1:numel(find(ind_r))
    mu_for_max(ipitch*ir) = (1-(ipitch./npitch)^2)*E./b(ir);
    pphi_for_max(ipitch*ir) = -1.6e-19*psi_midplane(ir)+g_z0(ir)./b(ir)*sqrt(2*mass*(E-mu_for_max(ipitch*ir)*b(ir)));
  end
end
n_mu_potato = 100;
mu_potato = linspace(0, max(mu_for_max), n_mu_potato);
pphi_potato = zeros(1, n_mu_potato);
for imu=2:numel(mu_potato)
  ind_mu = mu_for_max<mu_potato(imu)&mu_for_max>mu_potato(imu-1);
  if any(ind_mu)
    pphi_potato(imu) = max(pphi_for_max(ind_mu));
  end
end
pphi_potato(1) = pphi_potato(2);

data.pphi_potato = pphi_potato;
data.mu_potato = mu_potato;

%% Normalize if wanted
if p.norm
  Pphi_notnorm = Pphi;
  x_uptrapp_notnorm = x_uptrapp;
  x_downtrapp_notnorm = x_downtrapp;
  Pphi = (Pphi-1.6e-19*psiaxis)./(1.6e-19*(psiedge-psiaxis));
  x_uptrapp = (x_uptrapp-1.6e-19*psiaxis)./(1.6e-19*(psiedge-psiaxis));
  x_downtrapp = (x_downtrapp-1.6e-19*psiaxis)./(1.6e-19*(psiedge-psiaxis));

  lost_lfs_notnorm = lost_lfs;
  lost_hfs_notnorm = lost_hfs;
  touch_axis_notnorm = touch_axis;
  y_uptrapp_notnorm = y_uptrapp;
  y_downtrapp_notnorm = y_downtrapp;
  lost_lfs = lost_lfs*b_axis./E;
  lost_hfs = lost_hfs*b_axis./E;
  touch_axis = touch_axis*b_axis./E;
  y_downtrapp = y_downtrapp*b_axis./E;
  y_uptrapp = y_uptrapp*b_axis./E;
  
  pphi_potato_notnorm = pphi_potato;
  pphi_potato = (pphi_potato-19*psiaxis)./(1.6e-19*(psiedge-psiaxis));
  mu_potato_notnorm = mu_potato;
  mu_potato = mu_potato*b_axis./E;
  
  varnames=["Pphi", "x_uptrapp", "x_downtrapp", "y_uptrapp", "y_downtrapp",...
      "lost_lfs", "lost_hfs", "touch_axis", "pphi_potato", "mu_potato"];
    
  for kk=1:numel(varnames)
    eval(sprintf('data.%s = %s;', varnames(kk), varnames(kk)));
    eval(sprintf('data.notnorm.%s = %s_notnorm;', varnames(kk), varnames(kk)));      
  end
end


%% plot
if false
  figure; hold on;
  plot(Pphi, touch_axis)
  plot(Pphi, lost_lfs)
  plot(Pphi, lost_hfs)
  plot(x_uptrapp, y_uptrapp)
  plot(x_downtrapp, y_downtrapp)
  plot(pphi_potato, mu_potato);
%   ylim([0, 5e-13]);
  grid on;
  xlabel('P_\phi');
  ylabel('\mu');
end
%% plot
doplot=1;
if doplot
  if isempty(p.axx)
    figure; ax=gca;
  else
    ax=p.axx;
  end
  hold(ax, 'on');
  plot(ax,Pphi, touch_axis,color_in);
  plot(ax,Pphi, lost_lfs,color_in);
  plot(ax,Pphi, lost_hfs,color_in);

  plot(ax,x_uptrapp, y_uptrapp,color_in);
  plot(ax,x_downtrapp, y_downtrapp,color_in);
  plot(ax, min(x_downtrapp)*[1,1], [min(y_downtrapp), max(y_uptrapp)], [color_in ,'--'])
  plot(ax, pphi_potato, mu_potato, color_in)
  if ~exist('axx')
%     xlim(); 
    ylim([0,max(y_uptrapp)])
    grid on; box on;
  end
end
end