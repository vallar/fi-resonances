function data=resonances(varargin)
%% Function to compute analytical FI resonances
%
% INPUT (all optional)
%   eqdsk_fname (string): filename of the eqdsk
%   eqdsk_in (object): eqdsk object
%   E (float) (25e3): Energy in eV
%   npitch (int) (500): number of pitch values to scan
%   nr (int) (500): number of r values to scan
%   omega_exp (float) (150e3): frequency in Hz to check resonances against
%   n_exp (int) (2): experimental toroidal mode number
%   debug (bool) (0): set to 1 to print/plot debug quantities
%   norm (bool) (0): set to 1 to plot normalized quantities
%   p_range (int array) ([1:5]): array with p-values to scan
%   doplot (bool) (1): boolean variable to shut down plots
%   z0 (float) (1000): z0 to use where to compute the boundaries and
%   resonances (not working)
%   plot_split_t (bool) (0): plot the values of p in separate plots
%   color_in (char) ('k'): color to use for plotting
%
% OUTPUT
%   Structure with several useful information

%% Parse and check arguments
p = inputParser;
% Required
% addRequired(p,'E', @isnumeric);
% Optional
addParameter(p,'eqdsk_fname', '/home/vallar/WORK/scripts/matlab/fi-resonances/EQDSK_COCOS_02_POS.OUT',@ischar);
addParameter(p,'eqdsk_in',[], @isstruct);
addParameter(p,'E', 25e3,@isnumeric);
addParameter(p,'npitch', 500,@isnumeric);
addParameter(p,'nr', 500,@isnumeric);
addParameter(p,'omega_exp', 150e3,@isnumeric);
addParameter(p,'n_exp', 2,@isnumeric);
addParameter(p,'debug', 0,@isnumeric);
addParameter(p,'norm', 0,@isnumeric);
addParameter(p,'p_range', [1:5],@isnumeric);
addParameter(p,'doplot',1, @isnumeric);
addParameter(p,'z0',1000, @isnumeric);
addParameter(p,'plot_split_p',0, @isnumeric);

addParameter(p,'color_in','k', @ischar);
addParameter(p,'epot',0, @isnumeric);

parse(p, varargin{:})
p = p.Results;
data.input = p;
%% read eqdsk
if isempty(p.eqdsk_in)
  eq=read_eqdsk(p.eqdsk_fname);
else
  eq=p.eqdsk_in;
end
data.eq=eq;
%% select z where you want the resonances to be computed
if p.z0==1000
  z0=eq.zaxis;
else
  z0=p.z0;
end
%% computing rho, q at z=0, the R I want to scan
rho_psiN = sqrt((eq.psi-eq.psiaxis)./(eq.psiedge-eq.psiaxis)); rho_psiN=rho_psiN';
[~, ind_z0] = min(abs(eq.zmesh-z0));
rho_psiN_z0 = rho_psiN(ind_z0,:);
q_psiN_z0 = interp1(eq.rhopsi, eq.q, rho_psiN_z0);
ind_R_z0 = eq.rmesh<max(eq.rplas)& eq.rmesh>min(eq.rplas);
R_to_range = eq.rmesh(ind_R_z0);

%% compute B_phi from F function
g=eq.F;
g_2d = interpn(eq.rhopsi, abs(g), rho_psiN);
g_2d(isnan(g_2d)) = nanmin(nanmin(g_2d));
% Find g on the midplane and rho_pol_norm on the midplane
g_z0 = g_2d(ind_R_z0, ind_z0);
B_on_rgrid = g_z0./R_to_range;
[~, ind_r_axis] = min(abs(eq.rmesh(ind_R_z0)-eq.raxis));
b_axis = B_on_rgrid(ind_r_axis);

%% defining fast particles quantities and (R, pitch) grids
E=p.E;
if E>1,  E=E*1.6e-19; end
mass = 2*1.6e-27;
B0= abs(eq.b0); % This is the Bfield at r0, which is the center of the vacuum chamber
v = sqrt((2*E)./mass);
npitch=p.npitch;
nr=p.nr;
pitch_range = linspace(-1,1,npitch);
r_range = linspace(min(R_to_range), max(R_to_range), nr);
%Compute vphi
vphi = v*pitch_range;
data.pitch_range=pitch_range;
data.r_range=r_range;
%% map q on the r_range just defined
% ad-hoc COCOS transformation (so to have psi monotonically increasing from
% 0 to psiedge)
if eq.psiaxis<eq.psiedge && eq.psiaxis<0 
  psiedge = -1.*eq.psiaxis;
  psiaxis=0;
else
  psiedge=eq.psiedge;
  psiaxis=eq.psiaxis;
end
q_on_rgrid = interp1(eq.rmesh', q_psiN_z0, r_range);
q_on_rgrid(isnan(q_on_rgrid))=nanmax(q_on_rgrid);
B_on_rgrid = interp1(R_to_range', B_on_rgrid, r_range);
rho_range = interp1(eq.rmesh', rho_psiN_z0, r_range);
psi_range = psiedge*rho_range.^2;

%% Compute range in mu and Pphi
min_mu = 0;
max_mu = mass.*(v.^2)./(2*min(B_on_rgrid));
mu_range = linspace(min_mu, max_mu, npitch);

% This definition of Pphi is valid in a orthogonal right-handed system.
min_pphi = 2*min(-mass*r_range*max(vphi)-1.6e-19*(psi_range*(psiedge-psiaxis)+psiaxis));
max_pphi = max(mass*r_range*max(vphi)-1.6e-19*(psi_range*(psiedge-psiaxis)+psiaxis));
pphi_range = linspace(min_pphi, max_pphi, nr);
data.rho_range=rho_range;
data.mu_range=mu_range;
data.pphi_range=pphi_range;
%% computing frequencies
omega_pol_rvphi = zeros(nr, npitch);
omega_tor_rvphi = zeros(nr, npitch);
omega_pol_pphi_mu = zeros(nr, npitch);
omega_tor_pphi_mu = zeros(nr, npitch);
Pphi_values = zeros(nr, npitch);
for ii = 1:npitch
  for jj=1:nr
    omega_tor_rvphi(jj,ii) = abs(vphi(ii))./r_range(jj);
    omega_pol_rvphi(jj,ii) = abs(vphi(ii))./(q_on_rgrid(jj).*r_range(jj));
    % Computing Pphi and Mu so to know where to put such values in the
    % matrix on (Pphi, mu)
    Pphi_values(ii,jj) = mass*r_range(jj)*vphi(ii)-1.6e-19*psi_range(jj);
    [~,ind_pphi] = min(abs(Pphi_values(ii,jj)-pphi_range));
    mu_local = 1./(B_on_rgrid(jj))*(E-0.5*mass*vphi(ii)^2);
    [~,ind_mu] = min(abs(mu_local-mu_range));
    if omega_tor_pphi_mu(ind_pphi,ind_mu)~=0
      omega_tor_pphi_mu(ind_pphi,ind_mu) = max([omega_tor_rvphi(jj,ii), omega_pol_pphi_mu(jj,ii)]);
    else
      omega_tor_pphi_mu(ind_pphi,ind_mu) = omega_tor_rvphi(jj,ii);
    end
    if omega_tor_pphi_mu(ind_pphi,ind_mu)~=0
      omega_pol_pphi_mu(ind_pphi,ind_mu) = max([omega_pol_pphi_mu(ind_pphi,ind_mu), omega_pol_rvphi(jj,ii)]);
    else
      omega_pol_pphi_mu(ind_pphi,ind_mu) = omega_pol_rvphi(jj,ii);
      
    end
  end
end
data.omega_tor_rvphi = omega_tor_rvphi;
data.omega_pol_rvphi = omega_pol_rvphi;
data.omega_tor_pphi_mu = omega_tor_pphi_mu;
data.omega_pol_pphi_mu = omega_pol_pphi_mu;

%% debug correct mapping phi(R,V) and omega(R,V)
debug=p.debug;
if debug
  figure('Name', 'Debug plot');
  subplot(3,1,1);
  h=pcolor(r_range, pitch_range, Pphi_values');
  set(h, 'EdgeColor', 'none');
  xlabel('R'); ylabel('pitch'); title('Pphi=m*R*Vphi-z*psi'); colorbar();
  
  subplot(3,1,2);
  h=pcolor(r_range, pitch_range, omega_tor_rvphi');set(h, 'EdgeColor', 'none');
  xlabel('R'); ylabel('pitch'); title('omega_{TOR}=V/R'); colorbar();
  
  subplot(3,1,3);
  h=pcolor(pphi_range, mu_range, omega_tor_pphi_mu');set(h, 'EdgeColor', 'none');
  xlabel('P phi'); ylabel('mu'); title('omega_{TOR}=V/R'); colorbar();
end
%% Computing the resonance condition
omega_exp=p.omega_exp;
n=p.n_exp;
p_range = p.p_range;
% single n
if numel(n)==1
  res_rvphi=zeros(numel(p_range), nr, npitch);
  res_pphi_mu=zeros(numel(p_range), nr, npitch);
  for i=1:numel(p_range)
    res_rvphi(i,:,:) = omega_exp+i*omega_pol_rvphi-n*omega_tor_rvphi;
    res_pphi_mu(i,:,:) = omega_exp+i*omega_pol_pphi_mu-n*omega_tor_pphi_mu;
  end
  res_rvphi = 1./log(abs(res_rvphi));
  res_pphi_mu = 1./log(abs(res_pphi_mu));
  cumulative_res_rvphi = squeeze(sum(res_rvphi, 1));
  cumulative_res_pphi_mu = squeeze(sum(res_pphi_mu, 1));
  cumulative_res_pphi_mu(omega_tor_pphi_mu==0)=0;
elseif numel(n)>1
  res=zeros(numel(n), numel(p_range), nr, npitch);
  for i=1:numel(p_range)
    for j=n
      res(j, i,:,:) = omega_exp+i*omega_pol-j*omega_tor;
    end
  end
  res = 1./log(abs(res));
  cumulative_res = squeeze(sum(res, 1));
  cumulative_res = squeeze(sum(cumulative_res, 1));
end

data.cumulative_res_rvphi = cumulative_res_rvphi;
data.cumulative_res_pphi_mu = cumulative_res_pphi_mu;

%% normalize if needed
norm=p.norm;
if norm
  mu_range_notnorm = mu_range;
  mu_range = mu_range*b_axis./E;
  Pphi_range_notnorm = pphi_range;
  pphi_range = (pphi_range-1.6e-19*psiaxis)./(1.6e-19*(psiedge-psiaxis));
end

ccaxis=[0.6, 1]*0.1*numel(n)*numel(p_range);%(max(pitch_range)-min(pitch_range));
%% plot
if p.doplot
%% figure E, pitch
  figure('Name', 'rho-pitch'); 
  h=pcolor(rho_range, pitch_range,cumulative_res_rvphi');
  set(h, 'EdgeColor', 'none');
  title(sprintf('E=%i keV, \\omega_{EXP}=%i kHz, n=%i', E./1.6e-19*1e-3, omega_exp*1e-3, n));
  caxis(ccaxis)
  xlabel('\rho_\psi'); ylabel('\lambda');

%  Figure for double-check, but a bit useless now
%   f=figure(); 
%   subplot(111);
%   h=pcolor(-rho_range, 1-pitch_range.^2,cumulative_res_rvphi');
%   set(h, 'EdgeColor', 'none');
%   colorbar; caxis(ccaxis)
%   % colormap hot;
%   % xlim([0.44, 0.45]);
%   xlabel('-\rho_\psi'); ylabel('1-\lambda**2');
%% figure COM
  figure('Name', 'CoM'); 
  h=pcolor(pphi_range, mu_range, cumulative_res_pphi_mu');
  set(h, 'EdgeColor', 'none');
  title(sprintf('E=%i keV, \\omega_{EXP}=%i kHz, n=%i', E./1.6e-19*1e-3, omega_exp*1e-3, n));
  [eq]=boundaries_COM(64949, 1., E, 'eqdsk_in', eq, 'axx', gca(), 'color_in', 'w', 'norm', norm);
  caxis(ccaxis)
  set(gca,'FontSize',18)
%   colormap cool;
  % xlim([0.44, 0.45]);
  if ~norm
    xlabel('P_\phi'); ylabel('\mu');
  else
    xlabel('$\hat{P}_\phi$', 'interpreter','latex'); ylabel('$\hat{\mu}$', 'interpreter','latex');
  end
end
%%
if p.plot_split_p
if numel(n)==1
  for i=1:numel(p_range)
    figure('Name', num2str(i)); subplot(111);
    h=pcolor(pphi_range, mu_range, squeeze(res_pphi_mu(i,:,:))');
    set(h, 'EdgeColor', 'none');
    title(sprintf('p=%i', p_range(i)));
    colorbar hot; 
    caxis(ccaxis./numel(p_range));
  end
else
  for j=n
    for i=p
      figure('Name', num2str(i)); subplot(111);
      h=pcolor(Pphi_values', mu_range, squeeze(res(j, i,:,:)));
%       h=pcolor(rho_range, pitch_range, squeeze(res(j, i,:,:)));
      set(h, 'EdgeColor', 'none');
      title(sprintf('n=%i, p=%i', j,p(i)));
      colorbar; 
      caxis(ccaxis./numel(p));
%       xlim([0.45, 0.5]);
    end
  end  
end
end